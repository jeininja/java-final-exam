//Jei wen Wu - 1842614

package question3;

import java.util.Random;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class JavaFX extends Application{

	//Creating the random number generator object
	Random rand = new Random();
	
	@Override
	public void start(Stage stage) throws Exception {
		
		Group root = new Group();
		
		//Creating the elements
		Button heads = new Button("Heads");
		Button tails = new Button("Tails");
		TextField bet = new TextField();
		Text money = new Text("100");
		Text flipResult = new Text("");
		Text message = new Text ("");
		
		//Creating the containers
		VBox vbox = new VBox();
		HBox hbox = new HBox();
		
		//Adding the elements into the containers
		hbox.getChildren().addAll(heads, tails);
		vbox.getChildren().addAll(bet, hbox, money, flipResult, message);
		
		//Attaching the main element onto the Group object
		root.getChildren().addAll(vbox);
		
		//Setting up and displaying the scene
		Scene scene = new Scene(root, 300, 300);
		stage.setTitle("Coinflip Bet");
		stage.setScene(scene);
		stage.show();
		
		//Setting the eventListener on the heads button
		heads.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent arg0) {
				//Try+catch used to catch if the user enters invalid bet amount
				try {
					if(bet.getText() !="" && Integer.parseInt(bet.getText())<=Integer.parseInt(money.getText())) {
						message.setText("");
						//Flipping the coin, bet on heads, 1 = win, 0 = lose
						int coinflip = rand.nextInt(2);
						if(coinflip == 1) {
							int result = Integer.parseInt(bet.getText())+Integer.parseInt(money.getText());
							money.setText(Integer.toString(result));
							//Display what  happened this round
							flipResult.setText("Heads! You win "+ bet.getText());
						}
						else {
							int result = Integer.parseInt(money.getText())-Integer.parseInt(bet.getText());
							money.setText(Integer.toString(result));
							//Display what  happened this round
							flipResult.setText("Tails! You lose "+ bet.getText());
						}
					}
				}
				//Display message if invalid bet
				catch(Exception e){
					message.setText("Enter a valid input");
					flipResult.setText("");
				}
			}
		});
		
		//Setting the eventListener on the tails button
		tails.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent arg0) {
				//Try+catch used to catch if the user enters invalid bet amount
				try {
					if(bet.getText() !="" && Integer.parseInt(bet.getText())<=Integer.parseInt(money.getText())) {
						message.setText("");
						//Flipping the coin, bet on tails bet on heads, 1 = win, 0 = lose
						int coinflip = rand.nextInt(2);
						if(coinflip == 1) {
							int result = Integer.parseInt(bet.getText())+Integer.parseInt(money.getText());
							money.setText(Integer.toString(result));
							//Display what  happened this round
							flipResult.setText("Tails! You win "+ bet.getText());
						}
						else {
					int result = Integer.parseInt(money.getText())-Integer.parseInt(bet.getText());
					money.setText(Integer.toString(result));
					//Display what  happened this round
					flipResult.setText("Heads! You lose "+ bet.getText());
						}
					}
				}
				//Display message if invalid bet
				catch(Exception e){
					message.setText("Enter a valid input");
					flipResult.setText("");
				}
			}
		});	
	}
	
	//Launching the application
	public static void main (String[] args) {
		Application.launch(args);
	}
	
}
