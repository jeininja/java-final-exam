package question2;

public class Recursion {

	private static int counter = 0;
	private static int index = 0;
	
	public static void main(String[] args) {
		
		//Creating and filling in the array
		String[] words = new String[8];
		words[0]="walking";
		words[1]="walrus";
		words[2]="dog";
		words[3]="pizza";
		words[4]="email";
		words[5]="winter";
		words[6]="clown";
		words[7]="crown";
		
		int count = recursiveCount(words, 5);
		int count2 = recursiveCount2(words, 5);

		System.out.println(count);
		System.out.println(count2);
	}
	
	public static int recursiveCount(String[] words, int n) {
		
        if(index > words.length) {
            return 0;
        }

        if(index % 2 == 1 && words[index].contains("w") && words[index].length()>=n) {
            counter++;
        }
        
        index++;
        
        recursiveCount(words, n);
        
        return counter;
	}
    
	//This method is only used to compare the for loop version and the recursive version
	public static int recursiveCount2(String[] words, int n) {
		
		int count=0;
		
		for(int i =0; i<words.length; i++) {
			if(words[i].length()>=n && i%2==1 && words[i].contains("w")) {
				count++;
			}
		}
		
		return count;
	}
}
